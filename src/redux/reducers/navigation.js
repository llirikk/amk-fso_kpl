import {PUSH_TO_SCROLL_HISTORY, SET_NAVIGATION_HIDDEN_TYPE} from "../actionTypes";

export const navigation = (state = defaultState, action) => {
    if (action.type === SET_NAVIGATION_HIDDEN_TYPE) {
        return {
            ...state,
            hidden: action.payload.hidden,
        }
    } else if (action.type === PUSH_TO_SCROLL_HISTORY) {
        state.history[action.payload.layoutName] = [...state.history[action.payload.layoutName], action.payload.number];
        return {
            ...state,
        }
    } else {
        return state
    }
};

const defaultState = {
    hidden: false,
    history: {
        landing: []
    }
};
