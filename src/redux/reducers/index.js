import {combineReducers} from 'redux';
import {navigation} from './navigation';
import {animations} from "./animations";

export const rootReducer = combineReducers({
    animations,
    navigation,
});
