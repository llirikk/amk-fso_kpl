import {SET_INITIALIZED} from "../actionTypes";

export const animations = (state = defaultState, action) => {
    if (action.type === SET_INITIALIZED) {
        state[action.payload.animationName] = {
            initialized: action.payload.initialized
        };
        return {
            ...state
        }
    } else {
        return state
    }
};

const defaultState = {
    'header': {
        initialized: false
    },
    'screen1': {
        initialized: false
    },
};
