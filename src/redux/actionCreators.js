import {PUSH_TO_SCROLL_HISTORY, SET_INITIALIZED, SET_NAVIGATION_HIDDEN_TYPE} from "./actionTypes";

export const setNavigationHidden = (hidden) => ({
    type: SET_NAVIGATION_HIDDEN_TYPE,
    payload: {
        hidden
    }
});

export const setAnimationsInitialized = (animationName, initialized) => ({
    type: SET_INITIALIZED,
    payload: {
        initialized,
        animationName
    }
});

export const pushToScrollHistory = (layoutName, number) => ({
    type: PUSH_TO_SCROLL_HISTORY,
    payload: {
        layoutName,
        number
    }
});
