export const selectIsNavigationHidden = (state) => state.navigation.hidden;
export const selectIsAnimationsInitialized = (state, animationName) =>
    state.animations[animationName] && state.animations[animationName].initialized;
export const selectScrollHistoryByName = (state, name) => state.navigation.history[name];
export const selectIsScreenInView = (state, name, number) =>
    state.navigation.history[name][state.navigation.history[name].length - 1] === number;
