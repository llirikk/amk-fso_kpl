import logo from "../../assets/logo.svg";
import './Header.scss';
// import ReactExpandableListView from 'react-expandable-listview'
import HamburgerMenu from "../watchMenu/hamburgerMenu";
import React from "react";
import {
    selectIsAnimationsInitialized,
    selectIsNavigationHidden,
    selectIsScreenInView,
    selectScrollHistoryByName
} from "../../redux/selectors";
import {connect} from 'react-redux';
import {setAnimationsInitialized} from "../../redux/actionCreators";
import CSSTransition from "react-transition-group/CSSTransition";
import {slide as Menu} from 'react-burger-menu';

const mapStateToProps = (state) => ({
    scrollHistory: selectScrollHistoryByName(state, 'landing'), // need here for update component
    isFirstSlide: selectIsScreenInView(state, 'landing', 1),
    animationInitialized: selectIsAnimationsInitialized(state, 'header'),
    isHeaderHidden: selectIsNavigationHidden(state),
});

const Header = ({fullPageApi, dispatch, isHeaderHidden, animationInitialized, isFirstSlide}) => {

    const renderBurger = () => !isFirstSlide ? 
    <Menu noOverlay right isOpen={ false }>
      <div className='hamburger-menu-padding'>
        <HamburgerMenu />
      </div>
    </Menu> : null

    // if (isHeaderHidden) return null;

    const classNames = `${isFirstSlide ? 'logo-animation-first' : 'logo-animation '} ${animationInitialized ? 'full-opacity' : 'zero-opacity'}`;
    return (
        <div>
            <div
                className={`header ${isFirstSlide ? 'header-first' : 'header-first-animation'}`}>
                <div className='menu-items'>
                    <div>РУС</div>
                    <div>ENG</div>
                    <div>中文</div>
                    <div className='menu-item-right'>О ПРОЕКТЕ</div>
                    <div>ИСТОРИЯ</div>
                    <div>НОВОСТИ</div>
                    <div>МАГАЗИН</div>
                    <div>МЕРОПРИЯТИЯ</div>
                    <div>КОНТАКТЫ</div>
                </div>
            </div>
            <CSSTransition
                in={isFirstSlide && !animationInitialized}
                enter
                timeout={1500}
                className={classNames}
                classNames="logo-animation"
                onEntered={() => dispatch(setAnimationsInitialized('header', true))}
            >
                <div>
                    <img
                        className={`logo ${isFirstSlide ? 'logo-first' : ''}`}
                        onClick={() => {
                            fullPageApi && fullPageApi.moveTo(1);
                        }}
                        src={logo}
                        alt="logo"
                    />
                    {renderBurger()}
                </div>
            </CSSTransition>
        </div>
    );
};

export default connect(mapStateToProps)(Header)
