import './Section2.scss';
import chaika from "../../assets/chaika.png";
import zis101 from "../../assets/zis101.png";
import React from "react";
import DownButton from "../../containers/DownButton";
import UpButton from "../../containers/UpButton";
import {InView} from 'react-intersection-observer';
import CSSTransition from "react-transition-group/CSSTransition";
import {connect} from 'react-redux';
import {selectIsNavigationHidden, selectIsScreenInView, selectScrollHistoryByName} from "../../redux/selectors";
import {pushToScrollHistory} from "../../redux/actionCreators";

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

const mapStateToProps = (state) => ({ 
    navigationHidden: selectIsNavigationHidden(state),
    scrollHistory: selectScrollHistoryByName(state, 'landing'),
    isScrollInView: selectIsScreenInView(state, 'landing', 2)
});

const Section2 = ({fullPageApi, isScrollInView, dispatch}) => (
    
    <InView 
        as="div"
        threshold={0.005} 
        onChange={(viewing) => { viewing && dispatch(pushToScrollHistory('landing', 2)); }}
        className="screen"
    >
        <div className="chaikaZis">
            <UpButton fullPageApi={fullPageApi}/>
            <div style={{width: '100%'}}>
                <CSSTransition
                    in={isScrollInView}
                    timeout={1000}
                    appear
                    className="position position-lb screen2-chaika-animation"
                    classNames="position position-lb screen2-chaika-animation"
                >
                    <img className="" src={chaika} alt="chaika"/>
                </CSSTransition>

                <Hidden smDown>
                    <CSSTransition
                        in={isScrollInView}
                        timeout={1000}
                        className="position position-rb screen2-zis101-animation"
                        classNames="position position-rb screen2-zis101-animation"
                    >
                        <img className="" src={zis101} alt="zis101"/>
                    </CSSTransition>
                </Hidden>
            </div>

            <Container>

                <div className="title title-small title-offset">
                    КРЕМЛЬ 9
                </div>

                <Grid container>
                    <Grid item xs={12} sm={12} md={4} lg={4}></Grid>
                    <Grid item xs={12} sm={12} md={8} lg={8}>

                        <div className="text">
                            В январе 2021 года на ВДНХ откроется музей Гаража Особого
                            Назначения. 
                            <br/>
                            <br/>
                            Открытие приурочено к 100-летию Гаража, официально получившего
                            наименование
                            «Особый
                            Гараж» в
                            январе 1921 года – после издания Приказа No 13 по Управлению делами
                            Совета
                            народных
                            комиссаров. Эту дату, определяющую новое положение бывшего Собственного
                            Его
                            Императорского
                            Величества Гаража, или бывшей Автобазы Временного правительства, или
                            бывшего
                            Ленинского
                            гаража, и принято считать официальной датой рождения ГОНа.
                        </div>
                    </Grid>
                </Grid>

            </Container>

            <DownButton fullPageApi={fullPageApi}/>
        </div>
    </InView>

);

export default connect(mapStateToProps)(Section2);
