import './UpButton.scss';
import arrowUp from "../../assets/arrowUp.png";
import React from "react";
import {connect} from 'react-redux';
import {selectIsNavigationHidden} from "../../redux/selectors";
import {setNavigationHidden} from "../../redux/actionCreators";

const mapStateToProps = (state) => ({
    navigationHidden: selectIsNavigationHidden(state),
});

const UpButton = ({fullPageApi, navigationHidden, dispatch}) => {
    return (
        // <div className="arrow-up" style={navigationHidden ? {top: '2.5%'} : {top: '9.5%'}}
        <div className="arrow-up" style={{top: '9.5%'}}
             onClick={() => {
                 dispatch(setNavigationHidden(false));
                 fullPageApi.moveSectionUp()
             }}>
            <img className="arrow-up__png" src={arrowUp} alt="arrow-up"/>
        </div>
    );
};

export default connect(mapStateToProps)(UpButton);
