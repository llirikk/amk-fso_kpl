import './DownButton.scss'
import arrowDown from "../../assets/arrowDown.png";
import React from "react";
import {connect} from 'react-redux';
import {setNavigationHidden} from "../../redux/actionCreators";

const DownButton = ({dispatch, fullPageApi}) => (
    <div className="arrow-down" onClick={() => {
        dispatch(setNavigationHidden(true));
        fullPageApi.moveSectionDown();
    }}>
        <img className="arrow-down__png" src={arrowDown} alt="arrow-down"/>
    </div>
);

export default connect()(DownButton);
