import React from 'react';
import {connect} from 'react-redux'
import {selectIsNavigationHidden} from "../../redux/selectors";

const mapStateToProps = (state) => ({
    navigationHidden: selectIsNavigationHidden(state)
});

const LayoutWithNav = ({navigationHidden, Component, props}) => {
    return (
        <div className="section">
            <div className={navigationHidden ? "without-nav" : 'with-nav'}>
                <Component {...props}/>
            </div>
        </div>
    );
};

export default connect(mapStateToProps)(LayoutWithNav)
