import React, {useEffect, useState} from 'react'
import './App.scss'
import ReactFullpage from '@fullpage/react-fullpage';
import Section1 from "../Section1";
import Section2 from "../Section2";
import Section3 from "../Section3";
import Section4 from "../Section4";
import Section5 from "../Section5";
import Section6 from "../Section6";
import Header from "../Header";
import {connect} from 'react-redux';
import LayoutWithNav from "../../containers/Layout";
import {
    selectIsAnimationsInitialized,
    selectIsNavigationHidden,
    selectScrollHistoryByName
} from "../../redux/selectors";
import {setNavigationHidden} from "../../redux/actionCreators";

const mapStateToProps = (state) => ({
    navigationHidden: selectIsNavigationHidden(state),
    scrollHistory: selectScrollHistoryByName(state, 'landing'),
    isFirstScreenAnimationsInitialized: selectIsAnimationsInitialized(state, 'screen1'),
    isHeaderAnimationInitialized: selectIsAnimationsInitialized(state, 'header')
});

const App = (
    {scrollHistory, navigationHidden, dispatch}
) => {
    const [fullPageApi, setFullPageApi] = useState();

    useEffect(() => {
        window.fullpage_api.setKeyboardScrolling(false);
        window.fullpage_api.setAllowScrolling(false);
        setTimeout(() => {
            window.fullpage_api.setKeyboardScrolling(true);
            window.fullpage_api.setAllowScrolling(true);
        }, 1500);
        window.addEventListener('wheel', onScroll);
        return () => {
            window.removeEventListener('wheel', onScroll);
        }
    });

    const onScroll = () => {
        const lastScreenIndex = scrollHistory.length - 1;
        if (scrollHistory[lastScreenIndex] >= scrollHistory[lastScreenIndex - 1]) {
            if (!navigationHidden) {
                dispatch(setNavigationHidden(true))
            }
        } else {
            if (navigationHidden) {
                dispatch(setNavigationHidden(false))
            }
        }
    };

    return (
        <div className="App">
            <Header fullPageApi={fullPageApi}/>
            <Sections hasFullPageApiSet={!!fullPageApi} setFullPageApi={setFullPageApi}/>
        </div>
    );
};

const Sections = ({setFullPageApi, hasFullPageApiSet}) => {
    return (
        <ReactFullpage
            scrollingSpeed={1200}
            render={(allFullPageProps) => {
                if (!hasFullPageApiSet && allFullPageProps.fullpageApi) {
                    setFullPageApi(allFullPageProps.fullpageApi)
                }
                const fullPageApi = allFullPageProps.fullpageApi;
                return (
                    <ReactFullpage.Wrapper>
                        <LayoutWithNav Component={Section1} props={{fullPageApi}}/>
                        <LayoutWithNav Component={Section2} props={{fullPageApi}}/>
                        <LayoutWithNav Component={Section3} props={{fullPageApi}}/>
                        <LayoutWithNav Component={Section4} props={{fullPageApi}}/>
                        <LayoutWithNav Component={Section5} props={{fullPageApi}}/>
                        <LayoutWithNav Component={Section6} props={{fullPageApi}}/>
                    </ReactFullpage.Wrapper>
                )
            }
            }
        />
    );
};

export default connect(mapStateToProps)(App);
