import './Section1.scss'
import aurusFull from "../../assets/aurus_full.png";
import React, {useEffect} from "react";
import DownButton from "../../containers/DownButton";
import {selectIsAnimationsInitialized, selectIsScreenInView, selectScrollHistoryByName} from "../../redux/selectors";
import {InView} from "react-intersection-observer";
import {pushToScrollHistory, setAnimationsInitialized} from "../../redux/actionCreators";
import {connect} from "react-redux";
import CSSTransition from "react-transition-group/CSSTransition";
import {calculateCoordinatesPercent} from "../../helpers/calculateCoordinatesPercent";

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

const mapStateToProps = (state) => ({
    scrollHistory: selectScrollHistoryByName(state, 'landing'),
    isScrollInView: selectIsScreenInView(state, 'landing', 1),
    isAnimationsInitialized: selectIsAnimationsInitialized(state, 'screen1'),
});

const Section1 = ({fullPageApi, scrollHistory, isAnimationsInitialized, isScrollInView, dispatch}) => {
    useEffect(() => {
        const screen1 = document.getElementsByClassName('screen1-animation')[0];
        if (!screen1) return;
        screen1.addEventListener('mousemove', onMouseMove);
        return () => {
            const screen1 = document.getElementsByClassName('screen1-animation')[0];
            if (!screen1) return;
            screen1.removeEventListener('mousemove', onMouseMove);
        }
    });

    const onMouseMove = (event) => {
        moveAurusFront(event);
        moveAurusBack(event);
        moveKreml9(event);
    };

    const moveAurusFront = (event) => {
        if (window.innerWidth < 768) return;
        const aurusFrontImg = document.getElementsByClassName('first__aurus-front')[0];
        if (!aurusFrontImg) return;
        const {x, y} = calculateCoordinatesPercent(event);
        aurusFrontImg.style.objectPosition = `${(x * 0.01) - 13}vw ${(y * 0.01) + 4.5}vh`;
    };

    const moveAurusBack = (event) => {
        if (window.innerWidth < 768) return;
        const aurusBackImg = document.getElementsByClassName('first__aurus-back')[0];
        if (!aurusBackImg) return;
        const {x, y} = calculateCoordinatesPercent(event);
        aurusBackImg.style.objectPosition = `${(x * 0.01) + 18}vw ${(y * 0.01) + 4.5}vh`;
    };

    const moveKreml9 = (event) => {
        if (window.innerWidth < 768) return;
        const kreml9 = document.getElementsByClassName('screen1__creml9')[0];
        if (!kreml9) return;
        const {x, y} = calculateCoordinatesPercent(event);
        kreml9.style.marginLeft = x * 0.05 + 'vw';
        kreml9.style.marginBottom = (y * -0.0193) - 11 + 'vh';
    };

    return (
        <InView as="div" threshold={0.01} className="screen screen1__wrapper"
                onChange={(viewing) => {
                    viewing && dispatch(pushToScrollHistory('landing', 1));
                }}>
            <CSSTransition
                in={isScrollInView}
                timeout={{enter: 1300, exit: 500}}
                className={isAnimationsInitialized ? "screen1-animation" : "screen1-init-animation"}
                classNames={isAnimationsInitialized ? "screen1-animation" : "screen1-init-animation"}
                onEntered={() => dispatch(setAnimationsInitialized('screen1', true))}
            >
                <div className="screen1">
                    <div className="first__aurus">
                        <div className="title title-big">
                            КРЕМЛЬ 9
                        </div>
                        <div className="screen1__cars">
                            <img
                                className="first__aurus-front"
                                src={aurusFull}
                                style={{opacity: isScrollInView ? '1' : 0}}
                                alt="aurus-front"
                            />
                            <img
                                className="first__aurus-back"
                                src={aurusFull}
                                style={{opacity: isScrollInView ? '1' : 0}}
                                alt="aurus-back"
                            />
                        </div>
            
                    </div>
                    <DownButton fullPageApi={fullPageApi}/>
                </div>
            </CSSTransition>

            <Hidden smDown>
                <div className="fornote">
                    <div className="fornote--content fornote--content-big">
                        МУЗЕЙНО-ВЫСТАВОЧНЫЙ ТЕХНИЧЕСКИЙ ЦЕНТР АВТОМОБИЛЬНО-МОТОЦИКЛЕТНОГО КЛУБА
                        ФЕДЕРАЛЬНОЙ СЛУЖБЫ ОХРАНЫ РОССИЙСКОЙ ФЕДЕРАЦИИ
                    </div>
                </div>
            </Hidden>

        </InView>
    );
};

export default connect(mapStateToProps)(Section1);
