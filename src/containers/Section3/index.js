import './Section3.scss';
import React from "react";
import DownButton from "../../containers/DownButton";
import gaz12 from "../../assets/gaz12.png";
import map from "../../assets/map.png";
import UpButton from "../../containers/UpButton";
import {selectIsScreenInView, selectScrollHistoryByName} from "../../redux/selectors";
import {InView} from "react-intersection-observer";
import {pushToScrollHistory} from "../../redux/actionCreators";
import {connect} from "react-redux";
import CSSTransition from "react-transition-group/CSSTransition";

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

const mapStateToProps = (state) => ({
    scrollHistory: selectScrollHistoryByName(state, 'landing'),
    isScrollInView: selectIsScreenInView(state, 'landing', 3)
});

const Section3 = ({fullPageApi, isScrollInView, dispatch}) => (
    
    <InView 
        as="div"
        threshold={0.005} 
        onChange={(viewing) => { viewing && dispatch(pushToScrollHistory('landing', 3)); }}
        style={{backgroundImage: "url(" + map + ")"}}
        className="screen screen-map"
    >
            <UpButton fullPageApi={fullPageApi}/>

            <CSSTransition
                in={isScrollInView}
                timeout={1000}
                className="position position-lb screen3-gaz12-animation"
                classNames="position position-lb screen3-gaz12-animation"
            >
                <img src={gaz12} alt="gaz-12"/>
            </CSSTransition>

            <Container>

                <div className="title title-small title-offset">
                    КРЕМЛЬ&ndash;9
                </div>    

                <Grid container>
                    <Grid item xs={12} sm={12} md={5} lg={5}></Grid>
                    <Grid item xs={12} sm={12} md={6} lg={6}>
                        <div className="text">
                            Музейно-выставочный технический центр Гаража Особого Назначения ФСО
                            России языком авто- и мототехники расскажет о многих событиях ХХ-ХХI
                            вв. в истории нашей страны через призму одной из ключевых ее
                            спецслужб – службы государственной охраны. Посетители познакомятся
                            не только с историей Гаража особого назначения, но и с его
                            настоящим. Музей Гаража Особого Назначения станет современным
                            мультимедийным пространством, открытым для всех, кто испытывает
                            интерес к технической и политической истории России ХХ-ХХI вв.
                        </div>
                    </Grid>
                </Grid>

            </Container>

            <DownButton fullPageApi={fullPageApi}/>
        
    </InView>

);

export default connect(mapStateToProps)(Section3);