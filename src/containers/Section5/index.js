import './Section5.scss';
import React from "react";
import DownButton from "../../containers/DownButton";
import zis110 from "../../assets/zis110.png";
import UpButton from "../../containers/UpButton";
import {selectIsScreenInView, selectScrollHistoryByName} from "../../redux/selectors";
import {InView} from "react-intersection-observer";
import {pushToScrollHistory} from "../../redux/actionCreators";
import {connect} from "react-redux";
import CSSTransition from "react-transition-group/CSSTransition";

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

const mapStateToProps = (state) => ({
    scrollHistory: selectScrollHistoryByName(state, 'landing'),
    isScrollInView: selectIsScreenInView(state, 'landing', 5)
});

const Section5 = ({fullPageApi, isScrollInView, dispatch}) => (
    
    <InView 
        as="div"
        threshold={0.005} 
        onChange={(viewing) => { viewing && dispatch(pushToScrollHistory('landing', 5)); }}
        className="screen"
    >
            <UpButton fullPageApi={fullPageApi}/>

            <CSSTransition
                in={isScrollInView}
                timeout={1000}
                className="position position-lb zis110-animation"
                classNames="position position-lb zis110-animation"
            >
                <img src={zis110} alt="zis110"/>
            </CSSTransition>

            <Container>

                <div className="title title-small title-offset">
                    ПРОГРАММА
                </div>    

                <Grid container>
                    <Grid item xs={12} sm={12} md={5} lg={5}></Grid>
                    <Grid item xs={12} sm={12} md={6} lg={6}>
                    <div className="text">
                        В состав Центра войдёт и зона сменных экспозиций, в которой на регулярной основе будут
                        проводиться различные познавательные научно-популярные мероприятия, повествующие о некоторых
                        любопытных аспектах деятельности государственной охраны в разные периоды развития нашей страны.
                    </div>
                    </Grid>
                </Grid>

            </Container>

            <DownButton fullPageApi={fullPageApi}/>
        
    </InView>

);

export default connect(mapStateToProps)(Section5);
