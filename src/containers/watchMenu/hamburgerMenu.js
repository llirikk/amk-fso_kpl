import React from 'react';
import Expandable from 'react-expandable';
import styled from 'styled-components';
import './hamburgerMenu.css';

const Item = styled.div`
  display: flex;
  padding-left: 35px;	
  overflow-y: auto;
  font-size: 17px;
  text-align: left;	
  color: white;
`;

const ItemHeader = styled.div`
  display: flex;
  align-items: left;
  justify-content: space-between;
  width: 250px;
  padding-left:25px;
  padding-top: 10px;	
`;

const ItemTitle = styled.div`
  width: 170px;
  text-align: left;
  color: white;
  font-size: 17px !important;
  cursor: pointer;
  margin-bottom:1px;
`;

const ItemIcon = styled.div`
`;

export default function HamburgerMenu() {
	return (
  <Expandable
    headers={[
      ({ isOpened }) => (
        <ItemHeader>
          <ItemTitle>О ПРОЕКТЕ</ItemTitle>
          <ItemIcon>{isOpened ? '-' : '+'}</ItemIcon>
        </ItemHeader>
      ),
      ({ isOpened }) => (
        <ItemHeader>
          <ItemTitle>ИСТОРИЯ</ItemTitle>
          <ItemIcon>{isOpened ? '-' : '+'}</ItemIcon>
        </ItemHeader>
      ),
      ({ isOpened }) => (
        <ItemHeader>
          <ItemTitle>НОВОСТИ</ItemTitle>
          <ItemIcon>{isOpened ? '-' : '+'}</ItemIcon>
        </ItemHeader>
      ),
      ({ isOpened }) => (
        <ItemHeader>
          <ItemTitle>МАГАЗИН</ItemTitle>
          <ItemIcon>{isOpened ? '-' : '+'}</ItemIcon>
        </ItemHeader>
      ),
    ]}
    enableMultiOpen
  >
    <Item>
      first item<br />
      first item<br />
      first item<br />
    </Item>
    <Item>
      second item
    </Item>
    <Item>
      third item
    </Item>
  </Expandable>

)
}

