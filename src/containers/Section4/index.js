import './Section4.scss';
import React from "react";
import DownButton from "../../containers/DownButton";
import gaz2495 from "../../assets/gaz2495.png";
import UpButton from "../../containers/UpButton";
import {selectIsScreenInView, selectScrollHistoryByName} from "../../redux/selectors";
import {InView} from "react-intersection-observer";
import {pushToScrollHistory} from "../../redux/actionCreators";
import {connect} from "react-redux";
import CSSTransition from "react-transition-group/CSSTransition";


import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

const mapStateToProps = (state) => ({
    scrollHistory: selectScrollHistoryByName(state, 'landing'),
    isScrollInView: selectIsScreenInView(state, 'landing', 4)
});

const Section4 = ({fullPageApi, isScrollInView, dispatch}) => (
    
    <InView 
        as="div"
        threshold={0.005} 
        onChange={(viewing) => { viewing && dispatch(pushToScrollHistory('landing', 4)); }}
        className="screen"
    >
            <UpButton fullPageApi={fullPageApi}/>

            <Hidden smDown>
                <CSSTransition
                    in={isScrollInView}
                    timeout={1000}
                    className="position position-lb position-smaller screen4-gaz2459-animation"
                    classNames="position position-lb position-smaller screen4-gaz2459-animation"
                >
                    <img src={gaz2495} alt="gaz-2459"/>
                </CSSTransition>
            </Hidden>

            <Container>

                <div className="title title-small title-offset">
                    ЭКСПОНАТЫ
                </div>    

                <Grid container>
                    <Grid item xs={12} sm={12} md={4} lg={4}></Grid>
                    <Grid item xs={12} sm={12} md={8} lg={8}>
                        <div className="text">
                            Автомобили, на которых в течение всего ХХ века ездили руководители
                            государства – от последнего царя до первого постсоветского лидера, – составят основу постоянной
                            экспозиции музея. Помимо самих автомобилей, возивших императора Николая II, Ленина, Сталина,
                            Хрущева, Брежнева и других глав государства, в нее войдут и машины сопровождения, а также
                            мотоциклы из почетного эскорта, созданного в 1956 году и использовавшегося для выполнения задач
                            обеспечения торжественных встреч особо важных гостей и выдающихся людей своего времени:
                            космонавтов – Гагарина, Леонова с Терешковой, зарубежных лидеров – Тито, Фиделя Кастро,
                            Насера. 
                            <br/>
                            <br/>
                            <br/>
                            Автомобили Aurus (проект «Кортеж») – первые отечественные лимузины представительского класса за
                            все почти тридцать постсоветских лет – также станут частью постоянной экспозиции музея.
                        </div>
                    </Grid>
                </Grid>

            </Container>

            <DownButton fullPageApi={fullPageApi}/>
        
    </InView>

);

export default connect(mapStateToProps)(Section4);
