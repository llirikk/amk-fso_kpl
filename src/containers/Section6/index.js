import './Section6.scss'
import React from "react";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import {FormHelperText} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import zis101Front from '../../assets/zis101Front.png';
import gaz13 from '../../assets/gaz13.png';
import UpButton from "../../containers/UpButton";
import {selectIsScreenInView, selectScrollHistoryByName} from "../../redux/selectors";
import {InView} from "react-intersection-observer";
import {pushToScrollHistory} from "../../redux/actionCreators";
import {connect} from "react-redux";
import CSSTransition from "react-transition-group/CSSTransition";

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
// import chaika from "../../assets/chaika.png";
// import zis101 from "../../assets/zis101.png";
import DownButton from "../DownButton";

const mapStateToProps = (state) => ({
    scrollHistory: selectScrollHistoryByName(state, 'landing'),
    isScrollInView: selectIsScreenInView(state, 'landing', 6)
});

const Section6 = ({fullPageApi, isScrollInView, dispatch}) => {
    const renderEmailForm = () => (
        <FormControl className="screen6__mailForm">
            <Input
                className="mailInput__input"
                placeholder="email"
                id="email-input"
                startAdornment={
                    <InputAdornment className="mailInput__icon" position="start">
                        <AlternateEmailIcon className="mailInput__icon-svg" style={{color: "gray"}}/>
                    </InputAdornment>
                }
            />
            <FormHelperText style={{marginLeft: '0.7vw', color: 'gray', fontSize: '0.9vh'}}
                            htmlFor="email-input">email</FormHelperText>
            <Button variant="outlined"
                    className='sendMail__button'
                    style={{
                        opacity: '0.9',
                        fontFamily: 'ProximaNovaRegular, sans-serif',
                        fontSize: '1.9vh',
                        color: 'white',
                        border: '0.06vh solid gray',
                        backgroundColor: '#131416',
                        width: 'max-content'
                    }}
                    size="small"
            >
                Подписаться
            </Button>
        </FormControl>
    );

    return (

        <InView
            as="div"
            threshold={0.005}
            onChange={(viewing) => { viewing && dispatch(pushToScrollHistory('landing', 6)); }}
            className="screen"
        >
            <UpButton fullPageApi={fullPageApi}/>
                <Hidden smDown>
                    <CSSTransition
                        in={isScrollInView}
                        timeout={1000}
                        className="position position-lb position-small zis101-animation"
                        classNames="position position-lb position-small zis101-animation"
                    >
                        <img src={zis101Front} alt="zis-101-front"/>
                    </CSSTransition>
                </Hidden>

                <Hidden smDown>
                    <CSSTransition
                        in={isScrollInView}
                        timeout={1000}
                        className="position position-rb position-rb-resize gaz13-animation"
                        classNames="position position-rb position-rb-resize gaz13-animation"
                    >
                        <img src={gaz13} alt="gaz-13"/>
                    </CSSTransition>
                </Hidden>

            <Container>

                <div className="title title-small title-offset">
                    РАССЫЛКА
                </div>

                <Grid container>
                    <Grid item xs={12} sm={12} md={4} lg={4}></Grid>
                    <Grid item xs={12} sm={12} md={5} lg={5}>

                        <div className="text text-center">
                            Музей Гаража Особого Назначения станет современным мультимедийным
                            пространством, открытым для всех, кто испытывает интерес к технической и политической истории
                            России ХХ-ХХI вв.
                        </div>

                        {renderEmailForm()}

                        <Hidden smUp>
                            <CSSTransition
                                in={isScrollInView}
                                timeout={1000}
                                className="img gaz13-animation"
                                classNames="img gaz13-animation"
                            >
                                <img src={gaz13} alt="zis-101-front"/>
                            </CSSTransition>
                        </Hidden>

                    </Grid>
                </Grid>

            </Container>

            <div className="fornote">
                <div className="fornote--content fornote--content-small">
                    МУЗЕЙНО-ВЫСТАВОЧНЫЙ ТЕХНИЧЕСКИЙ ЦЕНТР АВТОМОБИЛЬНО-МОТОЦИКЛЕТНОГО КЛУБА
                    ФЕДЕРАЛЬНОЙ СЛУЖБЫ ОХРАНЫ РОССИЙСКОЙ ФЕДЕРАЦИИ
                </div>
            </div>

            <DownButton fullPageApi={fullPageApi}/>
        </InView>

    );
};

export default connect(mapStateToProps)(Section6);
