export const calculateCoordinatesPercent = ({clientY, clientX}) => {
    let width = window.innerWidth, height = window.innerHeight,
        currentPercentX = ((clientX * 100) / width),
        currentPercentY = ((clientY * 100) / height),
        xResult, yResult;
    if (currentPercentX <= 50) {
        if (currentPercentX === 50) {
            xResult = 0;
        }
        xResult = Math.abs(currentPercentX - 50).toFixed(1);
    } else {
        xResult = -(currentPercentX - 50).toFixed(1);
    }
    if (currentPercentY <= 50) {
        if (currentPercentY === 50) {
            yResult = 0;
        }
        yResult = -Math.abs(currentPercentY - 50).toFixed(1);
    } else {
        yResult = (currentPercentY - 50).toFixed(1);
    }
    return {
        x: parseFloat(xResult),
        y: parseFloat(yResult)
    };
};
