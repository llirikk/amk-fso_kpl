import React from 'react'
import App from './containers/App'
import {render} from 'react-dom'
import {rootReducer} from './redux/reducers'
import {createStore} from "redux";
import {Provider} from "react-redux";

const store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
const root = document.getElementById('root');

render(
    <Provider store={store}>
        <App/>
    </Provider>,
    root
);
